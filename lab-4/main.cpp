# include <mpi.h>
# include <cmath>
# include <cstdio>
# include <cstdlib>
# include <cstring>
# include <ctime>

#define MASTER 0
#define LEFT_TAG 0
#define RIGHT_TAG 1

#define INDEX(i, j, k) ((N * N) * (i) + (N) * (j) + (k) )

double L = 1.0;                 /* linear size of square region */
int N = 368;                    /* number of interior points per dim */
int arrayLen = N * N * N;

double *u, *u_new;              /* linear arrays to hold solution */

int currentBranch;              /* rank of this process */

int *i_min, *i_max;             /* min, max vertex indices of processes */
int *left_proc, *right_proc;    /* processes to left and right */

/*
  Functions:
*/
int main(int argc, char *argv[]);

double jacobi(int num_procs, const double *f, double h_x_2, double h_y_2, double h_z_2, double a);

void createIndex(int num_procs);

void timestamp();

double fun(double x, double y, double z) {
    return x * x + y * y + z * z;
}

double rho(double a, double x, double y, double z) {
    return 6 - a * fun(x, y, z);
}

int main(int argc, char *argv[]) {
    double norm, localNorm, delta, localDelta, wall_time, epsilon;
    double *f;
    int processNumber, iter, maxIter, i, j, k;

    const double h_x = L / N;
    const double h_y = L / N;
    const double h_z = L / N;

    const double h_x_2 = h_x * h_x;
    const double h_y_2 = h_y * h_y;
    const double h_z_2 = h_z * h_z;
    const double a = 1E+5;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &processNumber);
    MPI_Comm_rank(MPI_COMM_WORLD, &currentBranch);

    epsilon = 1E-08;
    maxIter = 1000;

    if (currentBranch == MASTER) {
        timestamp();
        printf("\n");
        printf("3-D Poisson equation using Jacobi algorithm:\n");
        printf("  ===========================================\n");
        printf("  1-D domains, non-blocking send/receive\n");
        printf("  Number of processes         = %d\n", processNumber);
        printf("  Number of interior vertices = %d\n", N);
        printf("  Accuracy = %.0e\n", epsilon);
        printf("\n");
    }

    createIndex(processNumber);

    u = new double[arrayLen];
    u_new = new double[arrayLen];
    f = new double[arrayLen];

    iter = 0;
    wall_time = MPI_Wtime();

    //  Инициализация начальных и граничных значений
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++)
            for (k = 0; k < N; k++) {
                if (i == 0 || j == 0 || k == 0 || i == N - 1 || j == N - 1 || k == N - 1) {
                    u[INDEX(i, j, k)] = fun(i * h_x, j * h_y, k * h_z);
                    u_new[INDEX(i, j, k)] = u[INDEX(i, j, k)];
                } else {
                    u[INDEX(i, j, k)] = 0.0;
                    u_new[INDEX(i, j, k)] = 0.0;
                }
                f[INDEX(i, j, k)] = rho(a, i * h_x, j * h_y, k * h_z);
            }
    }

    do {
        norm = 0.0;

        localNorm = jacobi(processNumber, f, h_x_2, h_y_2, h_z_2, a);
        MPI_Allreduce(&localNorm, &norm, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

        if (currentBranch == MASTER && iter % 5 == 0) {
            printf("  Step %4d  Norm = %g\n", iter, norm);
        }

        // swap
        for (i = i_min[currentBranch]; i <= i_max[currentBranch]; i++)
            for (j = 0; j < N; j++)
                for (k = 0; k < N; k++) {
                    u[INDEX(i, j, k)] = u_new[INDEX(i, j, k)];
                }
        iter++;
    } while (epsilon < norm && iter < maxIter);

    delta = 0;
    for (i = i_min[currentBranch]; i <= i_max[currentBranch]; i++)
        for (j = 0; j < N; j++)
            for (k = 0; k < N; k++) {
                localDelta = fmax(norm, fabs(u[INDEX(i, j, k)] - fun(i * h_x, j * h_y, k * h_z)));
            }

    MPI_Allreduce(&localDelta, &delta, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    wall_time = MPI_Wtime() - wall_time;

    if (currentBranch == MASTER) {
        printf("\n");
        printf("  Step %4d, Norm = %g, Delta = %g, time = %f secs\n", iter, norm, delta, wall_time);
        printf("Done\n");
        printf("\n");
    }

    delete f;
    delete u;
    delete u_new;
    delete i_min;
    delete i_max;
    delete left_proc;
    delete right_proc;
    MPI_Finalize();
    return 0;
}


double jacobi(int num_procs, const double *f, double h_x_2, double h_y_2, double h_z_2, double a) {
    int i, j, k, requests;
    double norm = 0.0;
    MPI_Request request[4];
    MPI_Status status[4];


    // асинхронный обмен граничных значений
    requests = 0;
    if (0 <= left_proc[currentBranch] && left_proc[currentBranch] < num_procs) {
        MPI_Irecv(u + INDEX(i_min[currentBranch] - 1, 0, 0), N * N, MPI_DOUBLE,
                  left_proc[currentBranch], LEFT_TAG, MPI_COMM_WORLD, request + requests++);

        MPI_Isend(u + INDEX(i_min[currentBranch], 0, 0), N * N, MPI_DOUBLE,
                  left_proc[currentBranch], RIGHT_TAG, MPI_COMM_WORLD, request + requests++);
    }

    if (0 <= right_proc[currentBranch] && right_proc[currentBranch] < num_procs) {
        MPI_Irecv(u + INDEX(i_max[currentBranch] + 1, 0, 0), N * N, MPI_DOUBLE,
                  right_proc[currentBranch], RIGHT_TAG, MPI_COMM_WORLD, request + requests++);

        MPI_Isend(u + INDEX(i_max[currentBranch], 0, 0), N * N, MPI_DOUBLE,
                  right_proc[currentBranch], LEFT_TAG, MPI_COMM_WORLD, request + requests++);
    }

    // Jacobi обновление для внутренних вершин в локальной области.
    for (i = i_min[currentBranch] + 1; i <= i_max[currentBranch] - 1; i++) {
        for (j = 1; j < N - 1; j++)
            for (k = 1; k < N - 1; k++) {
                u_new[INDEX(i, j, k)] = (
                                                1.0 / h_x_2 * (u[INDEX(i - 1, j, k)] + u[INDEX(i + 1, j, k)]) +
                                                1.0 / h_y_2 * (u[INDEX(i, j - 1, k)] + u[INDEX(i, j + 1, k)]) +
                                                1.0 / h_z_2 * (u[INDEX(i, j, k - 1)] + u[INDEX(i, j, k + 1)]) -
                                                f[INDEX(i, j, k)]
                                        ) / (2.0 * (1.0 / h_x_2 + 1.0 / h_y_2 + 1.0 / h_z_2) + a);

                norm = fmax(norm, fabs(u_new[INDEX(i, j, k)] - u[INDEX(i, j, k)]));
            }
    }

    // Дождаемся завершения всех неблокирующих сообщений.
    MPI_Waitall(requests, request, status);

    // Jacobi обновление для граничных вершин в локальной области.
    i = i_min[currentBranch];
    for (j = 1; j < N - 1; j++)
        for (k = 1; k < N - 1; k++) {
            u_new[INDEX(i, j, k)] = (
                                            1.0 / h_x_2 * (u[INDEX(i - 1, j, k)] + u[INDEX(i + 1, j, k)]) +
                                            1.0 / h_y_2 * (u[INDEX(i, j - 1, k)] + u[INDEX(i, j + 1, k)]) +
                                            1.0 / h_z_2 * (u[INDEX(i, j, k - 1)] + u[INDEX(i, j, k + 1)]) -
                                            f[INDEX(i, j, k)]
                                    ) / (2.0 * (1.0 / h_x_2 + 1.0 / h_y_2 + 1.0 / h_z_2) + a);
            norm = fmax(norm, fabs(u_new[INDEX(i, j, k)] - u[INDEX(i, j, k)]));
        }

    i = i_max[currentBranch];
    if (i != i_min[currentBranch]) {
        for (j = 1; j < N - 1; j++)
            for (k = 1; k < N - 1; k++) {
                u_new[INDEX(i, j, k)] = (
                                                1.0 / h_x_2 * (u[INDEX(i - 1, j, k)] + u[INDEX(i + 1, j, k)]) +
                                                1.0 / h_y_2 * (u[INDEX(i, j - 1, k)] + u[INDEX(i, j + 1, k)]) +
                                                1.0 / h_z_2 * (u[INDEX(i, j, k - 1)] + u[INDEX(i, j, k + 1)]) -
                                                f[INDEX(i, j, k)]
                                        ) / (2.0 * (1.0 / h_x_2 + 1.0 / h_y_2 + 1.0 / h_z_2) + a);
                norm = fmax(norm, fabs(u_new[INDEX(i, j, k)] - u[INDEX(i, j, k)]));
            }
    }
    return norm;
}


void createIndex(int num_procs) {
    double d, eps, x_max, x_min;
    int i, p;

    int *proc = new int[N];

    i_min = new int[num_procs];
    i_max = new int[num_procs];
    left_proc = new int[num_procs];
    right_proc = new int[num_procs];

    // Разделите диапазон [(1-eps) .. (N + eps)] равномерно между процессами.
    eps = 0.0001;
    d = (N - 1 + eps) / (double) num_procs;

    for (p = 0; p < num_procs; p++) {
        //Индексы I, присвоенные слою P, будут удовлетворять X_MIN <= I <= X_MAX.
        x_min = -eps + (double) (p * d);
        x_max = x_min + d;

        // Для узла с индексом I, сохранить в PROC [I] процесс P, к которому он принадлежит.
        for (i = 0; i < N; i++) {
            if (x_min <= i && i <= x_max) {
                proc[i] = p;
            }
        }
    }

    // Найти минимальный индекс I который связан с каждым процессом P.
    for (p = 0; p < num_procs; p++) {
        for (i = 0; i < N; i++) {
            if (proc[i] == p) {
                break;
            }
        }
        i_min[p] = i;

        // Найти самый максимальный индекс, связанный с каждым процессом P.
        for (i = N - 1; 0 < i; i--) {
            if (proc[i] == p) {
                break;
            }
        }
        i_max[p] = i;

        // Найдите процессы слева и справа.
        left_proc[p] = -1;
        right_proc[p] = -1;

        if (proc[p] != -1) {
            if (0 < i_min[p] && i_min[p] < N) {
                left_proc[p] = proc[i_min[p] - 1];
            }
            if (0 <= i_max[p] && i_max[p] < N - 1) {
                right_proc[p] = proc[i_max[p] + 1];
            }
        }
    }
    if (currentBranch == MASTER) {
        printf("\n  i_min = [");
        for (i = 0; i < num_procs; i++) {
            printf("%d ", i_min[i]);
        }
        printf("]\n");

        printf("  i_max = [");
        for (i = 0; i < num_procs; i++) {
            printf("%d ", i_max[i]);
        }
        printf("]\n");

        printf("  left_proc = [");
        for (i = 0; i < num_procs; i++) {
            printf("%d ", left_proc[i]);
        }
        printf("]\n");
        printf("  right_proc = [");
        for (i = 0; i < num_procs; i++) {
            printf("%d ", right_proc[i]);
        }
        printf("]\n\n");
    }
    delete[] proc;
}


void timestamp() {
# define TIME_SIZE 40

    static char time_buffer[TIME_SIZE];
    const struct tm *tm;
    time_t now;

    now = time(NULL);
    tm = localtime(&now);

    strftime(time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm);

    printf("%s\n", time_buffer);

    return;
# undef TIME_SIZE
}