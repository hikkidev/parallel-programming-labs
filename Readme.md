# Параллельное программирование

Лабораторные работы выполнены на виртуальной машине:

```
VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
Linux 5.3.0-29-generic
Memory:                          16 GiB System memory
Processor:                       AMD Ryzen 7 2700X Eight-Core Process
Architecture:                    x86_64
CPU op-mode(s):                  4-bit
Byte Order:                      Little Endian
Address sizes:                   48 bits physical, 48 bits virtual
CPU(s):                          8
On-line CPU(s) list:             0-7
Thread(s) per core:              1
Core(s) per socket:              8
Socket(s):                       1
NUMA node(s):                    1
Stepping:                        2
CPU MHz:                         3699.990
BogoMIPS:                        7399.98
Hypervisor vendor:               KVM
Virtualization type:             full
L1d cache:                       256 KiB
L1i cache:                       512 KiB
L2 cache:                        4 MiB
L3 cache:                        16 MiB
```

С использованием IDE - CLion 2019.3.4 build #CL-193.6494.38