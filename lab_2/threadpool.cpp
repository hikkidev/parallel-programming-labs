#include "threadpool.h"
#include <iostream>
#include <cassert>

namespace ThreadPoolNS {

    bool ThreadPool::Create(int numOfThreads) {
        assert(numOfThreads > 0);
        isRunning = true;
        awaitIntercepted = false;

        printf("[ThreadPool] OnCreate.\n");
        int res = pthread_mutex_init(&mutex, nullptr);
        if (res != 0) {
            printf("[ThreadPool] Mutex initialization error: %d \n", res);
            return false;
        }
        res = pthread_cond_init(&condVar, nullptr);
        if (res != 0) {
            printf("[ThreadPool] Condition var initialization error: %d \n", res);
            return false;
        }
        pthread_t tmpThread;
        for (int i = 0; i < numOfThreads; i++) {
            res = pthread_create(&tmpThread, nullptr, ThreadPool::Executor, this);
            if (res != 0) {
                printf("[ThreadPool] %d-th thread creation failed, error code: %d \n", i, res);
                Destroy();
                return false;
            }
            threads.push_back(tmpThread);
        }
        return true;
    }

    void ThreadPool::Await() {
        if (target > 0) {
            awaitIntercepted = true;
            printf("[ThreadPool] Waiting for Target completion.\n");
            for (auto thread : threads) {
                pthread_join(thread, nullptr);
            }
        } else {
            printf("[ThreadPool] The Target is not set. Nothing to await.\n");
        }
    }

    void ThreadPool::Destroy() {
        printf("[ThreadPool] OnDestroy.\n");
        isRunning = false;
        if (!awaitIntercepted) {
            awaitIntercepted = true;
            printf("[ThreadPool] Waiting for the completion of all Workers.\n");
            for (auto thread : threads) {
                pthread_join(thread, nullptr);
            }
        }

        threads.clear();
        queue.clear();
        pthread_mutex_destroy(&mutex);
        pthread_cond_destroy(&condVar);
        printf("[ThreadPool] Resource release.\n");
        printf("[ThreadPool] Stopped.\n");
    }

    void ThreadPool::SetTarget(int newTarget) {
        target = newTarget;
    }

    int ThreadPool::HowMuchCompleted() {
        return completedJobs;
    }

    void *ThreadPool::Executor(void *arg) {
        auto *pool = static_cast<ThreadPool *>(arg);
        pool->Handler();
        return nullptr;
    }

    void ThreadPool::Handler() {
        auto id = pthread_self();
        printf("[ThreadPool] Spawn new worker - TID: %ld \n", id);

        while (isRunning) {
            pthread_mutex_lock(&mutex);
            while (queue.empty()) pthread_cond_wait(&condVar, &mutex);

            auto task = queue.front();
            queue.pop_front();
            pthread_mutex_unlock(&mutex);

            auto list = task->Execute();
            PushAll(list);
            delete task;
        }
    }

    void ThreadPool::Push(IJob *newJob) {
        if (nullptr == newJob) return;
        pthread_mutex_lock(&mutex);
        if (target > 0) {
            if (completedJobs < target) {
                printf("[ThreadPool] Add new Job\n");
                queue.push_back(newJob);
            } else {
                printf("[ThreadPool] All %d Jobs are completed.\n", target);
                isRunning = false;
            }
        } else {
            printf("[ThreadPool] Add new Job\n");
            queue.push_back(newJob);
        }

        pthread_mutex_unlock(&mutex);
        pthread_cond_signal(&condVar);
    }

    void ThreadPool::PushAll(const std::vector<IJob *> &list) {
        if (list.empty()) return;
        pthread_mutex_lock(&mutex);

        if (target > 0) {
            if (completedJobs < target) {
                completedJobs++;
                printf("[ThreadPool] Add new %ld Jobs\n", list.size());
                for (auto job : list) queue.push_back(job);
            } else {
                printf("[ThreadPool] All %d Jobs are completed.\n", target);
                isRunning = false;
            }
        } else {
            completedJobs++;
            printf("[ThreadPool] Add new %ld Jobs\n", list.size());
            for (auto job : list) queue.push_back(job);
        }

        pthread_mutex_unlock(&mutex);
        pthread_cond_signal(&condVar);
    }

}





