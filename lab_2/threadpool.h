#ifndef __THREADPOOL_H__
#define __THREADPOOL_H__

#include <pthread.h>
#include <iostream>
#include <string>
#include <vector>
#include <list>

namespace ThreadPoolNS {

    class IJob {
    public:
        virtual std::vector<IJob *> Execute() = 0;

        virtual ~IJob() = default;
    };

    class ThreadPool {
    public:
        bool Create(int numOfThreads);

        void Await();

        void Destroy();

        void SetTarget(int newTarget);

        void Push(IJob *newJob);

        int HowMuchCompleted();

    private:
        static void *Executor(void *arg);

        void Handler();

        void PushAll(const std::vector<IJob *> &list);

    private:
        bool isRunning = true;
        bool awaitIntercepted = false;
        int completedJobs = 0;
        int target = -1;
        std::list<IJob *> queue;
        std::vector<pthread_t> threads;
        pthread_cond_t condVar;
        pthread_mutex_t mutex;
    };

} // namespace ThreadPoolNS
#endif

