#include "threadpool.h"
#include <cstdio>
#include <cstring>
#include <unistd.h>
#include <curl/curl.h>
#include <regex.h>
#include <vector>
#include <algorithm>

#define DEFAULT_CUSTOM_BUFFER_SIZE 4096

const char *URL_PATTERN = R"(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*))";

// Буфер переменного размера
typedef struct {
    char *buff;
    size_t buffSize;
    size_t used;
} CustomBuffer;

bool initializeBuffer(CustomBuffer *buffer) {
    buffer->used = 0;
    buffer->buffSize = 0;
    buffer->buff = (char *) malloc(DEFAULT_CUSTOM_BUFFER_SIZE);

    if (buffer->buff == nullptr) return false;

    buffer->buffSize = DEFAULT_CUSTOM_BUFFER_SIZE;
    return true;
}

void freeMemoryBuffer(CustomBuffer *buffer) {
    if (buffer->buff != nullptr) free(buffer->buff);
}

// Дописывает текст буфер, при необходимости увеличивает его размер в двое
bool appendToBuffer(CustomBuffer *buffer, const char *data, size_t size) {
    size_t newSize = buffer->buffSize;

    while (buffer->used + size > newSize)
        newSize *= 2;

    if (newSize != buffer->buffSize) {
        char *newPtr = (char *) realloc(buffer->buff, newSize);
        if (!newPtr)
            return false;

        buffer->buff = newPtr;
        buffer->buffSize = newSize;
    }

    memcpy(buffer->buff + buffer->used, data, size);
    buffer->used += size;
    return true;
}

// Записывает ответ сервера в буфер переменного размера
size_t writeCallback(char *ptr, size_t size, size_t nmemb, void *data) {
    auto *buffer = (CustomBuffer *) data;
    size_t totalSize = size * nmemb;

    if (appendToBuffer(buffer, ptr, totalSize))
        return totalSize;
    else
        return 0;
}

class Job : public ThreadPoolNS::IJob {

private:
    std::string data;

public:
    Job() = default;;

    Job(char *url) {
        data = url;
    };

    std::vector<IJob *> Execute() override {
        auto id = pthread_self();
        printf("[TID: %ld] Parse: %s\n", id, data.c_str());

        size_t maxGroups = 1; // 1 - всё совпадение целиком; 2 - всё совпадение целиком и содержимое первой скобочной группы и т.д.
        size_t maxMatchesPerPage = 10;
        CURL *curlHandle;
        CURLcode rCode;
        CustomBuffer buffer;
        regex_t patternBuffer;
        regmatch_t groupArray[maxGroups];
        std::vector<IJob *> result;

        if (!initializeBuffer(&buffer)) {
            printf("Initialize Custom Buffer failed\n");
            return result;
        }

        curlHandle = curl_easy_init();
        if (curlHandle) {
            curl_easy_setopt(curlHandle, CURLOPT_URL, data.c_str());
            curl_easy_setopt(curlHandle, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, writeCallback);
            curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &buffer);

            // Посылаем запрос
            rCode = curl_easy_perform(curlHandle);
            // Проверяем ответ
            if (rCode != CURLE_OK) {
                freeMemoryBuffer(&buffer);
                curl_easy_cleanup(curlHandle);
                printf("Request failed: %s\n", curl_easy_strerror(rCode));
                return result;
            }

            // В конец буфера записываем нулевой байт
            if (!appendToBuffer(&buffer, "", 1)) {
                printf("Final appendToBuffer failed\n");
                freeMemoryBuffer(&buffer);
                return result;
            }

            // Создаем регулярное выражение
            if (regcomp(&patternBuffer, URL_PATTERN, REG_EXTENDED)) {
                printf("Could not compile regular expression.\n");
                return result;
            };

            // Извлекаем URLs из страницы
            char *cursor = buffer.buff;
            for (int m = 0; m < maxMatchesPerPage; m++) {
                if (regexec(&patternBuffer, cursor, maxGroups, groupArray, 0)) break; // No more matches

                unsigned int group = 0;
                unsigned int offset = 0;
                for (group = 0; group < maxGroups; group++) {
                    if (groupArray[group].rm_so == (size_t) -1) break;  // No more groups

                    if (group == 0) offset = groupArray[group].rm_eo;

                    // Вычленяем URL
                    char cursorCopy[strlen(cursor) + 1];
                    strcpy(cursorCopy, cursor);
                    cursorCopy[groupArray[group].rm_eo] = 0;
                    char *url = cursorCopy + groupArray[group].rm_so;

                    result.push_back(new Job(url));
                    //printf("Match %u, Group %u: [%2u-%2u]: %s\n", m, group, groupArray[group].rm_so, groupArray[group].rm_eo, url);
                }
                cursor += offset;
            }

            // Формируем путь к новому файлу
            std::string filename = *new std::string(data);
            std::replace(filename.begin(), filename.end(), '/', '-');
            std::replace(filename.begin(), filename.end(), '.', '-');
            std::string path = std::string("../pages/") + filename;

            // Сохраняем страницу в файл
            FILE *file = fopen(path.c_str(), "ab+");
            int results = fputs(buffer.buff, file);
            if (results == EOF) {
                printf("Failed to write file.\n");
            }

            // Освобождаем ресурсы
            fclose(file);
            curl_easy_cleanup(curlHandle);
            freeMemoryBuffer(&buffer);
            regfree(&patternBuffer);
            return result;
        }
        return result;
    }
};

int main(int argc, char **argv) {
    ThreadPoolNS::ThreadPool threadPool;

    threadPool.Create(5);
    threadPool.SetTarget(25);

    auto *job = new Job("http://127.0.0.1:8000/test.txt");
    threadPool.Push(job);

    threadPool.Await();
//    while (threadPool.HowMuchCompleted() < 20) {
//        sleep(1);
//    }
    threadPool.Destroy();
    return 0;
}