#include <omp.h>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include "../Timer.cpp"

using namespace std;

// f
double fun(double x, double y) {
    return sin(x) * cos(y);
}

// u
double phi(double x, double y) {
    return -2 * sin(x) * cos(y);
}

int main(int argc, char **argv) {

// Входные данне
    const int N = (argc > 1 ? atoi(argv[1]) : 100); // количество узлов в сетке, по каждой из координат области
    const double eps = (argc > 2 ? atof(argv[2]) : 1e-8);
    const int maxIter = (argc > 3 ? atoi(argv[3]) : 1000);

// Прямоугольная сетка, имеет шаг [h_x] пооси 𝑥 и шаг [h_y] пооси 𝑦 и определана на единичном квадрате
    const double h_x = 1.0 / N;
    const double h_x_2 = h_x * h_x;
    const double h_y = 1.0 / N;
    const double h_y_2 = h_y * h_y;

    double norm;
    int iter = 0;
    auto timer = Timer();
    vector<double> result(N * N), result_next(N * N), f(N * N);

    timer.newTimeStamp();
//  Инициализация начальных и граничных значений
#pragma omp parallel for // NOLINT(openmp-use-default-none)
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {

            if (i == 0 || j == 0 || i == N - 1 || j == N - 1) {
                result[i * N + j] = fun(i * h_x, j * h_y);
            } else {
                result[i * N + j] = 0.0;
            }
            f[i * N + j] = phi(i * h_x, j * h_y);
        }
    }

    do {
        norm = 0.0;
//  Численное решение уравнения Пуассона на основе конечно-разностного приближения
#pragma omp parallel for reduction(max:norm) // NOLINT(openmp-use-default-none)
        for (int i = 1; i < N - 1; i++) {
            for (int j = 1; j < N - 1; j++) {
                result_next[i * N + j] = (
                                                 h_y_2 * (result[(i - 1) * N + j] + result[(i + 1) * N + j]) +
                                                 h_x_2 * (result[i * N + j - 1] + result[i * N + j + 1]) -
                                                 h_x_2 * h_y_2 * f[i * N + j]
                                         ) / (2.0 * (h_x_2 + h_y_2));

                norm = fmax(norm, fabs(result_next[i * N + j] - result[i * N + j]));
            }
        }
//  Сохранение вычисленного результата
#pragma omp parallel for // NOLINT(openmp-use-default-none)
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                result[i * N + j] = result_next[i * N + j];
            }
        }
        iter++;
    } while ((norm > eps) && (iter < maxIter));

// Log
    cout << "Norm:\t" << norm << endl << "Iter:\t" << iter << endl << "Time:\t" << timer.elapsedSec() << endl << endl;
//    for (int i = 0; i < N; i++) {
//        for (int j = 0; j < N; j++)
//            cout << result[i * N + j] << "\t";
//        cout << endl;
//    }
    return 0;
}