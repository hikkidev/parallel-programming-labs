#pragma once

#include <vector>
#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <cstring>
#include <future>
#include "Future.h"
#include "matrix.h"
#include "utils.cpp"
#include "../Timer.cpp"

/**
 * Функция вызывает переданный ей блок кода в отдельном потоке (используя POSIX Threads)
 *
 * @param block - функция
 * @param args - аргументы функции [block]
 * */
template<typename R, typename T, typename... Args>
Future<R> async(T &&block, Args &&... args) {
    pthread_t threadId;
    auto future = new Future<R>();

    auto params = new auto([=] {
        return block(args...);
    });

    auto startRoutine = [](void *input) -> void * {
        auto callable = reinterpret_cast<decltype(params)>(input);

        R *res = new R();
        *res = (*callable)();
        delete callable;
        return res;
    };

    int err = pthread_create(&threadId, nullptr, startRoutine, params);

    if (err) {
        std::cout << "Thread creation failed : " << strerror(err);
        delete future;
        delete params;
    } else {
        future->setThreadId(threadId);
    }
    return *future;
}

/**
 * Функция умножает матрицу на вектор используя OpenMP.
 * Декомпозиция на k-блоков по строкам матрицы, где k = @param threadsNumber
 *
 * @param A - матрица
 * @param x - вектор
 * @param threadsNumber - кол-во потоков
 * @return - результат умножения [A] * [x] (вектор)
 * */
template<typename T>
std::vector<T> multiplyOmp(matrix<T> &A, std::vector<T> &x, const int threadsNumber) {
    std::vector<T> out(A.size, 0);

#pragma omp parallel for shared(A, x, out) num_threads(threadsNumber) // NOLINT(openmp-use-default-none)
    for (int i = 0; i < A.size; i++) {
        T sum = T(0);
        for (int j = 0; j < A.size; j++) {
            sum += A[i * A.size + j] * x[j];
        }
        out[i] = sum;
    }

    return out;
}

/**
 * Функция умножает матрицу на вектор используя std::async/std::future.
 * Декомпозиция на k-блоков по строкам матрицы, где k = @param threadsNumber
 *
 * @param A - матрица
 * @param x - вектор
 * @param threadsNumber - кол-во потоков
 * @return - результат умножения [A] * [x] (вектор)
 * */
template<typename T>
std::vector<T> multiplyStd(matrix<T> &A, std::vector<T> &x, const int threadsNumber) {
    std::vector<T> out(A.size, 0);
    std::vector<std::future<void>> futures;
    futures.reserve(threadsNumber);

    auto lambdaCompute = [&A, &x, &out, &threadsNumber](const int jobId) {
        const int sectionSize = A.size / threadsNumber;
        const int rest = A.size % threadsNumber;

        int startRow, endRow;

        // First thread does more job
        if (jobId == 0) {
            startRow = sectionSize * jobId;
            endRow = (sectionSize * (jobId + 1)) + rest;
        } else {
            startRow = sectionSize * jobId + rest;
            endRow = (sectionSize * (jobId + 1)) + rest;
        }

        for (int i = startRow; i < endRow; ++i) {
            const int row = i * A.size;

            T sum = T(0);
            for (int j = 0; j < A.size; ++j) {
                sum += A[row + j] * x[j];
            }
            out[i] = sum;
        }
    };

    for (int i = 0; i < threadsNumber; i++) {
        futures.push_back(std::async(std::launch::async, lambdaCompute, i));
    }

    for (auto &task: futures) task.get();

    futures.clear();
    return out;
}

/**
 * Функция умножает матрицу на вектор используя @code Future<R> async(T &&block, Args &&... args)
 * Декомпозиция на k-блоков по строкам матрицы, где k = @param threadsNumber
 *
 * @param A - матрица
 * @param x - вектор
 * @param threadsNumber - кол-во потоков
 * @return - результат умножения [A] * [x] (вектор)
 * */
template<typename T>
std::vector<T> multiplyMy(matrix<T> &A, std::vector<T> &x, const int threadsNumber) {
    std::vector<T> out(A.size, 0);
    std::vector<Future<T>> futures;
    futures.reserve(threadsNumber);

    auto lambdaCompute = [&A, &x, &out, &threadsNumber](const int jobId) {
        const int sectionSize = A.size / threadsNumber;
        const int rest = A.size % threadsNumber;

        int startRow, endRow;

        // First thread does more job
        if (jobId == 0) {
            startRow = sectionSize * jobId;
            endRow = (sectionSize * (jobId + 1)) + rest;
        } else {
            startRow = sectionSize * jobId + rest;
            endRow = (sectionSize * (jobId + 1)) + rest;
        }

        for (int i = startRow; i < endRow; ++i) {
            const int row = i * A.size;

            T sum = T(0);
            for (int j = 0; j < A.size; ++j) {
                sum += A[row + j] * x[j];
            }
            out[i] = sum;
        }
        return T(0);
    };

    for (int jobId = 0; jobId < threadsNumber; jobId++) {
        futures.push_back(async<T>(lambdaCompute, jobId));
    }

    for (auto &task: futures) task.get();
    futures.clear();
    return out;
}

int main(int argc, char **argv) {

    const int size = (argc > 1 ? atoi(argv[1]) : 10);
    const int threads = (argc > 2 ? atoi(argv[2]) : 2);
    const int type = (argc > 3 ? atoi(argv[3]) : 3);
    auto timer = Timer();

    auto A = matrix<double>(size);
    auto x = std::vector<double>(size);
    std::vector<double> b;
    initializer(A, x);

    std::cout << "Matrix size: " << size << std::endl
              << "Threads count: " << threads << std::endl << std::endl
              << "Start multiplication" << std::endl;

    switch (type) {
        case 1:
            std::cout << "\tOpenMP" << std::endl;
            timer.newTimeStamp();
            b = multiplyOmp(A, x, threads);
            std::cout << "\tElapsed time = " << timer.elapsedSec() << " sec" << std::endl;
            break;
        case 2:
            std::cout << "\tSTD Async/Future" << std::endl;
            timer.newTimeStamp();
            b = multiplyStd(A, x, threads);
            std::cout << "\tElapsed time = " << timer.elapsedSec() << " sec" << std::endl;
            break;
        case 3:
            std::cout << "\tCustom Async/Future" << std::endl;
            timer.newTimeStamp();
            b = multiplyMy(A, x, threads);
            std::cout << "\tElapsed time = " << timer.elapsedSec() << " sec" << std::endl;
            break;
        default:
            std::cout << "\tSequential" << std::endl;
            timer.newTimeStamp();
            b = A * x;
            std::cout << "\tElapsed time = " << timer.elapsedSec() << " sec" << std::endl;
            break;
    }


//    std::cout << "A = [\n";
//    A.print();
//    std::cout << "]\n";
//
//    std::cout << "x = [";
//    printVector(x);
//    std::cout << "]\n";

//    std::cout << "b = [";
//    printVector(b);
//    std::cout << "]\n";
    return 0;
}