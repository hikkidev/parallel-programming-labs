#include <vector>
#include <iostream>
#include "matrix.h"

template<typename T>
void printVector(std::vector<T> v) {
    for (auto item : v) {
        std::cout << item << " ";
    }
}

template<typename T>
void initializer(matrix<T> &A, std::vector<T> &x) {
    for (int i = 0; i < A.size; i++) {
        for (int j = 0; j < A.size; j++) {
            A[i * A.size + j] = i * A.size + j;
        }
    }

    for (int i = 0; i < A.size; i++) {
        x[i] = A.size - i;
    }
}
