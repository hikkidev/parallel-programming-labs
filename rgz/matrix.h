#include <cstdlib>
#include <vector>
#include <iostream>

#ifndef RGZ_MATRIX_H
#define RGZ_MATRIX_H


template<typename T>
class matrix {
public:
    int size = 0;

    matrix(int size) {
        this->size = size;
        values.resize(size * size);
    }

    ~matrix() {
        values.clear();
    }

    T &operator[](const int index) {
        return values[index];
    }

    std::vector<T> operator*(const std::vector<T> x) {
        if (size != x.size()) { throw "Dimensions A and x do not match"; }
        std::vector<T> out(size, 0);

        for (int i = 0; i < size; i++) {
            T sum = T(0);
            for (int j = 0; j < size; j++) {
                sum += values[i * size + j] * x[j];
            }
            out[i] = sum;
        }

        return out;
    }

    void print() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                std::cout << "\t" << values[i * size + j];
            }
            std::cout << std::endl;
        }
    }

private:
    std::vector<T> values;
};


#endif //RGZ_MATRIX_H
