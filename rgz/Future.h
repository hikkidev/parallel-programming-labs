#include <pthread.h>

#ifndef RGZ_FUTURE_H
#define RGZ_FUTURE_H


template<typename T>
class Future {
public:
    void setThreadId(pthread_t &id) {
        threadId = id;
    }

    T get() {
        pthread_join(threadId, &value);
        return *((T *) (value));
    }

private:
    pthread_t threadId;
    void *value;
};


#endif //RGZ_FUTURE_H
