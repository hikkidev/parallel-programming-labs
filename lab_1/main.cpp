#include<cstdio>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<pthread.h>
#include <err.h>
#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <stdexcept>
#include <array>

#define SUCCESS 0

struct Bundle {
    int counter;
    int client_fd;

    Bundle(int c, int fd) : counter(c), client_fd(fd) {}

};

bool INFO_LOG = false;

//the thread function
void *connection_handler(void *);

int main(int argc, char *argv[]) {
    printf("PID: %d\n\n", getpid());

    struct sockaddr_in server, client;

    int sock, client_fd, hConnections = 0, port;
    socklen_t sin_len = sizeof(client);
    pthread_t thread_id;
    pthread_attr_t attr;


    std::stringstream convert(argv[1]);

    if (!(convert >> port)) port = 8080;
    std::cout << "The server is running on the port: " << port << '\n';


    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attr, 512 * 1024 * 1024);

    //Create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) err(1, "can't open socket");
    else puts("Socket created");


    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);


    //Bind
    if (bind(sock, (struct sockaddr *) &server, sizeof(server)) == -1) {
        close(sock);
        err(1, "Can't bind");
    }
    puts("Bind socket - done");

    //Listen
    //Number of connections that can be waiting while another finishes
    listen(sock, SOMAXCONN);

    //Accept and incoming connection
    puts("Waiting for incoming connections...");

    while ((client_fd = accept(sock, (struct sockaddr *) &client, &sin_len))) {
        hConnections++;
        if (INFO_LOG) printf("Connection №%d accepted.\n", hConnections);

        if (pthread_create(&thread_id, &attr, connection_handler, new Bundle(hConnections, client_fd)) < 0) {
            printf("Connection №%d refused.", hConnections);
            continue;
        }
    }

    return 0;
}

std::string execute(const char *cmd) {
    std::array<char, 128> buffer{};
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

/*
 * This will handle connection for each client
 * */
void *connection_handler(void *arg) {
    if (INFO_LOG) printf("[TID: %ld] onCreate\n", pthread_self());

    Bundle *bundle = ((Bundle *) arg);

    std::stringstream response, response_body;
    const int max_client_buffer_size = 1024;
    int result, client_fd = bundle->client_fd;
    char buf[max_client_buffer_size];

    result = recv(client_fd, buf, max_client_buffer_size, 0);

    if (result == -1) {
        printf("[TID: %ld] Recv failed: %d.\n", pthread_self(), result);
    } else if (result == 0) {
        if (INFO_LOG) printf("[TID: %ld] Connection №%d closed by client.\n", pthread_self(), bundle->counter);
    } else if (result > 0) {
        buf[result] = '\0';

        // Build body (HTML)
        response_body << "<title>Test C++ HTTP Server</title>\n"
                      << "<h1>Request data:</h1>\n"
                      << "php version: "
                      << execute("php script.php")
                      << "<p>Headers</p>\n"
                      << "<pre>" << buf << "</pre>\n"
                      << "</br>\n";

        // Build response: headers + body
        response << "HTTP/1.1 200 OK\r\n"
                 << "Version: HTTP/1.1\r\n"
                 << "Content-Type: text/html; charset=utf-8\r\n"
                 << "Content-Length: " << response_body.str().length()
                 << "\r\n\r\n"
                 << response_body.str();

        result = write(client_fd, response.str().c_str(), response.str().length());
        if (result < SUCCESS) {
            // Error
            printf("[TID: %ld] Send response failed.\n", pthread_self());
        }
        // Close connection
        if (INFO_LOG) printf("[TID: %ld] Close connection.\n", pthread_self());
        shutdown(client_fd, SHUT_WR);
        close(client_fd);
    }

    if (INFO_LOG) printf("[TID: %ld] onDestroy\n", pthread_self());
    delete bundle;
    return nullptr;
}