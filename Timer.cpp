#include <chrono>

class Timer {
private:
    // Псевдонимы типов используются для удобного доступа к вложенным типам
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_t> timestamp;

public:
    void newTimeStamp() {
        timestamp = clock_t::now();
    }

    double elapsedSec() const {
        return std::chrono::duration_cast<second_t>(clock_t::now() - timestamp).count();
    }
};


